angular.module("empresasWeb").factory("loginAPI", function($http){
	var _login = function (usuario){
		return $http({
   			method: "POST",
    		url: "http://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
    		data:{
    			"email" : usuario.email,
  				"password" : usuario.password
    		},
    		headers: {'Content-Type': 'application/json'}
		});
	};
	return {
		login: _login
	}
});