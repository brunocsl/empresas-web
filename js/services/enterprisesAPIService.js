angular.module("empresasWeb").factory("enterprisesAPI", function($http, $rootScope){
  
  var _getEnterprises = function (){
    return $http({
      method: "GET",
      url: "http://empresas.ioasys.com.br/api/v1/enterprises",
      headers: {
        "Content-Type": "application/json",
        "acess-token" : $rootScope.dadosAcesso["access-token"],
        "client" : $rootScope.dadosAcesso["client"],
        "uid" :   $rootScope.dadosAcesso["uid"]
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
          console.log(error);
          throw error;
          return false;
    });
  };

  var _getEnterprise = function(id){
    console.log("entrou");
    return $http({
      method: "GET",
      url: "http://empresas.ioasys.com.br/api/v1/enterprises/" + id,
      headers: {
        "Content-Type": "application/json",
        "acess-token" : $rootScope.dadosAcesso["access-token"],
        "client" : $rootScope.dadosAcesso["client"],
        "uid" :   $rootScope.dadosAcesso["uid"]
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
      throw error;
      return false;
    });
  };

  return {
    getEnterprises: _getEnterprises,
    getEnterprise: _getEnterprise
  };
});
