angular.module("empresasWeb").controller("loginCtrl", function ($scope, $rootScope, loginAPI, $location){

	$scope.login = function(usuario){
		loginAPI.login(usuario).then(function(data){
			$rootScope.dadosAcesso = {
				"access-token" : data.headers()["access-token"],
				"client" : data.headers()["client"],
				"uid" : data.headers()["uid"]
			};
			$location.path("/home")
			
		});
	};
});
