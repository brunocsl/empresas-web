angular.module("empresasWeb").controller("homeCtrl", function($scope){
	$scope.enterprises = [ {
		code : 1,
		name : "Empresa 1",
		country: "Brasil",
		business:"Agro",
	}, {
		code : 2,
		name : "Empresa 2",
		country: "França",
		business:"Aviation",
	}, {
		code : 3,
		name : "Empresa 3",
		country: "Chile",
		business:"Biotech",
	}, {
		code : 4,
		name : "Empresa 4",
		country: "Japão",
		business:"Eco",
	}, {
		code : 5,
		name : "Empresa 5",
		country: "Marrocos",
		business:"Ecommerce",
	}, {
		code : 6,
		name : "Empresa 6",
		country: "Canadá",
		business:"Education",
	}, {
		code : 7,
		name : "Empresa 7",
		country: "Alemanha",
		business:"Fashion",	
	} ];
});