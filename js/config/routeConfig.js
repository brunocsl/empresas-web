angular.module("empresasWeb").config(function ($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	
	$routeProvider

	.when("/", {
		templateUrl: "views/login.html",
		controller: "loginCtrl"
	})

	.when("/home", {
		templateUrl: "views/home.html",
		controller: "homeCtrl",
		// resolve: {
		// 	enterprises: function (enterprisesAPI) {
		// 		return enterprisesAPI.getEnterprises();
		// 	}

		// }
	})

	.when("/detalhesEmpresa/:id", {
		templateUrl: "views/detalhesEmpresa.html",
		controller: "detalhesEmpresaCtrl",
		// resolve: {
		// 	enterprise: function (enterprisesAPI, $route) {
		// 	return enterprisesAPI.Enterprise($route.current.params.id);
		// 	}
		// }
	})
	
	$routeProvider.otherwise({redirectTo: "/"});

});
